import { call } from "./Api.js";
import { productsGen } from "./Product.js";

const productsHtml = document.getElementById("catalog");

// fill up catalog

const fillUpCatalog = async (search = null) => {
  let url = "products";

  const productList = await call(url);

  let result = ``;
  for (const product of productList) {
    if (search == null) {
      result += productsGen(product);
    } else if (product.title.toUpperCase().includes(search.toUpperCase())) {
      result += productsGen(product);
    }
  }
  productsHtml.innerHTML = result;
};

fillUpCatalog();

// sort function

const sortFunction = async (sort = null) => {
  let url = "products";

  if (sort == "asc") {
    url += "?sort=asc";
  } else if (sort == "desc") {
    url += "?sort=desc";
  }

  const productList = await call(url);

  let result = ``;
  for (const product of productList) {
    result += productsGen(product);
  }
  productsHtml.innerHTML = result;
};

// sort call with event listener

const sort = document.getElementById("sort");
sort.addEventListener("change", () => sortFunction(sort.value));

// search

const search = document.getElementById("searchQuery");
const searchButton = document.getElementById("search-button");

searchButton.addEventListener("click", () => fillUpCatalog(search.value));
